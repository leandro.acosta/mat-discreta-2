#include "Wahlaan/Rii.h"
#include <time.h>
#define REPETIR 333
#define REPETIR_BIP 0

double t, ttotal;
u32 rand1, rand2;
u32 minimum_color = 1000000000, test = 0;

void ordennatural(Grafo g) {
    t = clock();
    OrdenNatural(g);
    Greedy(g);
    assert(EsPropio(g));
    printf("OrdenNatural: %d \n", NumeroDeColores(g));
    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("Tiempo: %lf\n\n", t);
}

void rmbcnormal(Grafo g, bool swtch) {
    t = clock();
    //OrderDump(g);
    u32 color = NumeroDeColores(g);
    RMBCnormal(g);
    //OrderDump(g);
    Greedy(g);
    u32 newcolor = NumeroDeColores(g);
    assert(newcolor <= color);
    printf("pass\n");
    assert(EsPropio(g));
    printf("RMBnormal: %d \n", NumeroDeColores(g));
    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("Tiempo: %lf\n\n", t);
    
    srand(clock());
    rand1=rand()%NumeroDeColores(g), rand2=rand()%NumeroDeColores(g);
    printf("a: %u ,b %u\n",rand1,rand2);
    if (swtch) SwitchColores(g, rand1, rand2);

    minimum_color = (minimum_color > NumeroDeColores(g) ? NumeroDeColores(g): minimum_color);
}

void rmbcchicogrande(Grafo g, bool swtch) {

    t = clock();
    u32 color = NumeroDeColores(g);
    RMBCchicogrande(g);
    Greedy(g);
    u32 newcolor = NumeroDeColores(g);
    assert(newcolor <= color);
    assert(EsPropio(g));
    printf("RMBchicogrande: %d \n", NumeroDeColores(g));
    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("%d. Tiempo: %lf\n\n", test, t);

    srand(clock());
    rand1=rand()%NumeroDeColores(g), rand2=rand()%NumeroDeColores(g);
    if (swtch) SwitchColores(g, rand1, rand2);

    minimum_color = (minimum_color > NumeroDeColores(g) ? NumeroDeColores(g): minimum_color);


}

void welshpowell(Grafo g, bool swtch) {

    t = clock();
    OrdenWelshPowell(g);
    Greedy(g);
    assert(EsPropio(g));
    printf("WelshPowell: %d\n", NumeroDeColores(g));
    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("Tiempo: %lf\n\n", t);
    
    srand(clock());
    u32 rand1=rand()%NumeroDeVertices(g), rand2=rand()%NumeroDeVertices(g);
    if (swtch) SwitchVertices(g, rand1, rand2);
    minimum_color = (minimum_color > NumeroDeColores(g) ? NumeroDeColores(g): minimum_color);
}

void rmbcrevierte(Grafo g, bool swtch) {
    t = clock();
    u32 color = NumeroDeColores(g);
    RMBCrevierte(g);
    Greedy(g);
    u32 newcolor = NumeroDeColores(g);
    assert(newcolor <= color);
    assert(EsPropio(g));
    printf("RMBrevierte: %d \n", NumeroDeColores(g));
    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("Tiempo: %lf\n\n", t);
    
    srand(clock());
    rand1=rand()%NumeroDeColores(g), rand2=rand()%NumeroDeColores(g);
    if (swtch) SwitchColores(g, rand1, rand2);
    minimum_color = (minimum_color > NumeroDeColores(g) ? NumeroDeColores(g): minimum_color);
}

bool checkbipartitness(Grafo g) {
    bool b = Bipartito(g);

    // for(int _ = 0; _ < REPETIR_BIP; _++) {
    //     assert(b == Bipartito(g));
    // }

    assert(EsPropio(g));
    return b;
}


int main() {
    printf("Construccion del grafo \n");
    
    t = clock();
    ttotal = clock();

    Grafo g = ConstruccionDelGrafo();
    // g = CopiarGrafo(G);

    if (g == NULL) {
        printf("La copia es NULL\n");
        return 0;
    } else {
        printf("Se copio bien\n");
    }

    t = (double)(clock() - t) / CLOCKS_PER_SEC;
    printf("Tiempo construccion grafo: %lf\n", t);

    u32 last = NombreDelVertice(g,0);
    for(u32 i = 1; i< NumeroDeVertices(g);i++){
        u32 vertice = NombreDelVertice(g,i);
        if(vertice < last){
            printf("WRONG in %u\n",i);
            break;
        }
        last = vertice;
    }

    for(test=0; test < REPETIR; test++) {
        ordennatural(g);
        welshpowell(g, true);
        for(int _ = 0; _ < 1000; _++) {
            rmbcnormal(g, true);
            rmbcchicogrande(g, true);
            rmbcrevierte(g, true);
        }
    }

    printf("\n\nMinimo numero de colores: %u\n", minimum_color);
    ttotal = (double)(clock() - ttotal) / CLOCKS_PER_SEC;
    printf("\nTiempo total: %lf\n\n", ttotal);

    
    if (checkbipartitness(g)) {
        printf("Es Bipartito\n");
    } else {
        printf("No es Bipartito\n");
    }

    DestruccionDelGrafo(g);
    // DestruccionDelGrafo(G);
    
}
