#ifndef _STACK_H
#define _STACK_H
#include "Rii.h"

typedef struct _stack_t * stack_t;

stack_t create_stack();
void empty_stack(stack_t s);
stack_t push(int e, stack_t s);
int top(stack_t s);
void pop(stack_t s);
bool is_empty_stack(stack_t s);
bool is_full_stack(stack_t s);
void destroy_stack(stack_t s);

#endif
