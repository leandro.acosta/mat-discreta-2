#ifndef _GRAFO_H
#define _GRAFO_H
#include "Rii.h"

typedef struct GrafoSt* Grafo;
typedef unsigned int u32;

/* Funciones De Construccion/Destruccion/Copia del grafo */
Grafo ConstruccionDelGrafo();
Grafo CopiarGrafo(Grafo);
void DestruccionDelGrafo(Grafo);

/* Funciones de coloreo */
u32 Greedy(Grafo G);
int Bipartito(Grafo);


/* Funciones para extraer informacion de datos del grafo */
u32 NumeroDeVertices(Grafo);
u32 NumeroDeLados(Grafo);
u32 NumeroDeColores(Grafo);


/* Funciones de los vertices */
u32 NombreDelVertice(Grafo G, u32 i);
u32 NumeroDelVertice(Grafo G, u32 i);
u32 ColorDelVertice(Grafo, u32);
u32 GradoDelVertice(Grafo, u32);
u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);
u32 NombreJotaesimoVecino(Grafo G,u32 i, u32 j);


/* Funciones de ordenacion */
char OrdenNatural(Grafo G);
char OrdenWelshPowell(Grafo G);
char SwitchVertices(Grafo G,u32 i,u32 j);
char RMBCnormal(Grafo G);
char RMBCrevierte(Grafo G);
char RMBCchicogrande(Grafo G);
char SwitchColores(Grafo G,u32 i,u32 j);


/* Funciones extra */
void OrderDump(Grafo);
u32 CantidadNodosConColor(Grafo, u32);
bool EsPropio(Grafo G);

#endif