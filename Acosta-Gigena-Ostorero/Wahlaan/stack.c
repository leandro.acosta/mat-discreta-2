#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include "stack.h"

/* node_t the type of pointers to nodes
*/
typedef struct _node_t * node_t;

/* _node_t the type of nodes
*/
struct _node_t {
    int elem;
    node_t next;
};

/* one more indirection to deal with language c parameter passing properties
*/
typedef struct _stack_t {
    node_t the_stack;
} _stack_t;

stack_t create_stack() {
    stack_t s = (stack_t)malloc(sizeof(_stack_t));
    if(!s)
        return NULL;
    s->the_stack = NULL;
    return (s);
}

void empty_stack(stack_t s) {
    if(!s)
        return;
    while (!is_empty_stack(s))
        pop(s);
}

/* pre: !is_full_stack(s) */
stack_t push(int e, stack_t s) {
    node_t node = (node_t)malloc(sizeof(struct _node_t));
    if(!node)
        return NULL;
    node->next = s->the_stack;
    node->elem = e;
    s->the_stack = node;
    return s;
}

/* pre: !is_empty_stack(s) */
int top(stack_t s) {
    assert(!is_empty_stack(s));
    return s->the_stack->elem;
}

/* pre: !is_empty_stack(s) */
void pop(stack_t s) {
    assert(!is_empty_stack(s));
    node_t node = s->the_stack;
    s->the_stack = s->the_stack->next;
    free(node);
    node = NULL;
}

bool is_empty_stack(stack_t s) {
    return s->the_stack == NULL;
}

bool is_full_stack(stack_t s) {
    return (s == NULL && s != NULL);  //always returns false (trick to make the compiler happy)
}

void destroy_stack(stack_t s) {
    while (!is_empty_stack(s)) {
        pop(s);
    }
    free(s);
}

