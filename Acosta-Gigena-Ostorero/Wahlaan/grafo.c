#include "Rii.h"
#include <assert.h>

typedef uint32_t u32;

typedef struct GrafoSt {
  u32 n;                  // Cantidad de nodos.
  u32 m;                  // Cantidad de aristas.
  u32 delta;              // Grado mas grande
  u32 c_propio;           // Color en el orden actual
  u32 *orden;             // Orden en el que se va a ejecutar Greedy
  u32 *orden_real;        // Orden real para OrdenNatural
  u32 *nombre_real;       // En la posicion i, esta el nombre real del nodo i
  vector *vecinos;        // vecinos[i] vector con vecinos del nodo i
  vector n_color;         // n_color[c] cantidad de nodos con color c
  u32 *color;             // color[i] color actual del nodo i
} GrafoSt;


/** 
  * Devuelve false si no se pudo alocar memoria suficiente, en caso contrario
  * devuelve true.
  */
bool checkspace(Grafo G) {
  return G->vecinos && G->orden && G->orden_real && G->color && G->nombre_real;
}

/**
  * Funciones De Construccion/Destruccion/Copia del Grafo
  */
Grafo ConstruccionDelGrafo() {
  Grafo G = (Grafo)malloc(sizeof(GrafoSt));
  G->n = 0;
  G->m = 0;
  G->delta = 0;
  G->c_propio = G->n;
  
  // Saltea todas las lineas que empiecen con c de comentario.
  char* line = NULL;
  line = get_line();
  while(line[0] == 'c'){
    free(line);
    line = get_line(); 
  }

  // Verifica que la primera linea sin comentario empiece con p edge
  if (!checkline(line, true)) {
    DestruccionDelGrafo(G);
    printf("error en primera linea sin comentario\n");
    return NULL;
  }

  // Recibo la cantidad de nodos y vertices del input
  get_numbers(line, 7, &(G->n), &(G->m));
  free(line);

  // Aloco memoria
  G->vecinos = calloc(G->n,sizeof(vector));
  G->orden = calloc(G->n,sizeof(u32));
  G->orden_real = calloc(G->n,sizeof(u32));
  G->nombre_real = calloc(G->n,sizeof(u32));
  G->color = calloc(G->n,sizeof(u32));
  
  if(!checkspace(G)){
    DestruccionDelGrafo(G);
    return NULL;
  }

  G->n_color = vector_init();
  if(!G->n_color){
    DestruccionDelGrafo(G);
    return NULL;
  }


  for(u32 i = 0; i < (G->n); i++) {
    G->vecinos[i] = vector_init();
    G->color[i] = -1;
  }

  // Inicializo nuestro arbol balanceado, donde la key va a ser el nombre real
  // en el rango [0, 1^32) y el value es el mapeo a nuestro nodo, que va
  // a estar en el rango [i, n)
  avlnode* avl = avl_init();
  
  // Aca llevo el conteo de nodos que voy mapeando hasta el momento   
  u32 cantidad_de_nodos = 0;

  // Vector con los nombres reales de los nodos del input
  vector nodos = vector_init();
  if(!nodos){
    DestruccionDelGrafo(G);
    return NULL;
  }

  // Lineas leidas hasta el momento
  u32 lines = 0;
  for(u32 i = 0; i < G->m; i++) {
    u32 v, u;
    line = get_line();

    // Verifica que la primera linea empiece con "e "
    if (checkline(line, false)) {
      get_numbers(line, 2, &u, &v);
      
      // Si no existe en el arbol balanceado, lo agrega, y lo pushea en nodos.
      if (!exists_in_avl(avl, u)) {
        avl = avl_insert(avl, u, cantidad_de_nodos);
        if(!vector_add(nodos, u)){
          DestruccionDelGrafo(G);
          return NULL;
        }
        G->nombre_real[cantidad_de_nodos] = u;
        cantidad_de_nodos++;
      }

      if (!exists_in_avl(avl, v)) {
        avl = avl_insert(avl, v, cantidad_de_nodos);
        if(!vector_add(nodos, v)){
          avl_destroy(avl);
          DestruccionDelGrafo(G);
          return NULL;
        }
        G->nombre_real[cantidad_de_nodos] = v;
        cantidad_de_nodos++;
      }
      
      // Si la cantidad de nodos es distinto a G->n, libero memoria y retorno NULL
      if (cantidad_de_nodos > G->n) {
        printf("cantidad de vertices leidos no es la declarada\n");
        DestruccionDelGrafo(G);
        vector_free(nodos);
        avl_destroy(avl);
        free(line);
        return NULL;
      }
      
      // assert(cantidad_de_nodos < G->n);
      // Agrego los vecinos.
      // printf("%u %u\n", find_avl_value(avl, v), find_avl_value(avl, u));
      if(!vector_add(G->vecinos[find_avl_value(avl, v)], find_avl_value(avl, u))){
        avl_destroy(avl);
        DestruccionDelGrafo(G);
        return NULL;
      }
      if(!vector_add(G->vecinos[find_avl_value(avl, u)], find_avl_value(avl, v))){
        avl_destroy(avl);
        DestruccionDelGrafo(G);
        return NULL;
      }
      lines++;
    } else {
      // Si no empieza con "e ", hay un error en el input, entonces libero
      // memoria y retorno NULL. 
      printf("error de lectura en lado %u\n", lines+1);
      DestruccionDelGrafo(G);
      vector_free(nodos);
      avl_destroy(avl);
      free(line);
      return NULL;
    }
    free(line);
  }

  // Si hay mas de m lineas, libero memoria, y retorno NULL.
  line = get_line();
  if (line[0]) {
    printf("error de lectura en lado %u\n", G->m+1);
    DestruccionDelGrafo(G);
    vector_free(nodos);
    avl_destroy(avl);
    free(line);
    return NULL;
  }

  // Si la cantidad de nodos es distinto a G->n, libero memoria y retorno NULL
  if (cantidad_de_nodos != G->n) {
    printf("cantidad de vertices leidos no es la declarada\n");
    DestruccionDelGrafo(G);
    vector_free(nodos);
    avl_destroy(avl);
    free(line);
    return NULL;
  }

  // Hago un sort para el orden_real, para OrdenNatural
  vector_sort(nodos);
  for(u32 i = 0; i < G->n; i++) {
    G->orden_real[i] = find_avl_value(avl, vector_get(nodos, i));
  }

  // Libero memoria
  vector_free(nodos);
  avl_destroy(avl);
  OrdenNatural(G);
  free(line);
  for(u32 i = 0; i < G->n;i++){
    u32 d = GradoDelVertice(G,i);
    if(G->delta < d)
      G->delta = d;
  }

  Greedy(G);

  return G;
}

/** 
  * Dado un Grafo G, alloco memoria para un grafo nuevo g_nuevo, y copio
  * todos los datos de G en g_nuevo. 
  * Devuelve NULL si no se pudo alocar suficiente memoria.
  */
Grafo CopiarGrafo(Grafo G) {
  if (G == NULL) return NULL;
  Grafo g_nuevo = (Grafo)malloc(sizeof(GrafoSt));
  if (g_nuevo == NULL) return NULL;
  g_nuevo->n = G->n;
  g_nuevo->m = G->m;
  g_nuevo->delta = G->delta;
  g_nuevo->vecinos = (vector*)malloc((G->n) * sizeof(vector));
  g_nuevo->orden = (u32*)malloc((G->n) * sizeof(u32));
  g_nuevo->orden_real = (u32*)malloc((G->n) * sizeof(u32));
  g_nuevo->nombre_real = (u32*)malloc((G->n) * sizeof(u32));
  g_nuevo->color = (u32*)malloc((G->n) * sizeof(u32));
  g_nuevo->c_propio = G->c_propio;

  if (!checkspace(g_nuevo)) {
    DestruccionDelGrafo(g_nuevo);
    return NULL;
  }

  memcpy(g_nuevo->orden, G->orden, sizeof(u32) * G->n);
  memcpy(g_nuevo->orden_real, G->orden_real, sizeof(u32)* G->n);
  memcpy(g_nuevo->nombre_real, G->nombre_real, sizeof(u32)* G->n);
  memcpy(g_nuevo->color, G->color, sizeof(u32) * G->n);
  g_nuevo->n_color = vector_copy(G->n_color);

  for(u32 i = 0; i < NumeroDeVertices(g_nuevo); i++) {
    g_nuevo->vecinos[i] = vector_copy(G->vecinos[i]);
  }

  return g_nuevo;
}

void DestruccionDelGrafo(Grafo G) {
  if(!G)
    return;
  if(G->vecinos){
    for(u32 i = 0; i < G->n; i++) {
      vector_free(G->vecinos[i]);
    }
    free(G->vecinos);
  }
  if (G->n_color) vector_free(G->n_color);
  if (G->orden_real) free(G->orden_real);
  if (G->nombre_real) free(G->nombre_real);
  if (G->color) free(G->color);
  if (G->orden) free(G->orden);
  free(G);
}

/* Funciones de coloreo */

u32 Greedy(Grafo G) {
  u32 n = G->n;
  if(!n)
    return 0;
  
	bool* vis = calloc(n, sizeof(bool));
  if(!vis){
    return (u32)~0;
  }
  vector usados = vector_init();
  if(!usados){
    free(vis);
    return (u32)~0;
  }
  //Se recolorean todos los nodos como 2^32 - 1
  //un color imposible para marcar que aun 
  //no se ha coloreado ese nodo
  memset(G->color, ~0, sizeof(u32)*n);
  vector_clear(G->n_color);
  G->c_propio = 0;

  if(!vector_add(G->n_color,0)){
    free(vis);
    return (u32)~0;
  }
  for(u32 i = 0; i < n; i++) {
    //Se busca el nodo en el i-esimo lugar
    u32 node = G->orden[i];
    u32 grado = vector_size(G->vecinos[node]);
    for(u32 j = 0; j < grado; j++) {
      //Se itera por los vecinos
      u32 vecino = vector_get(G->vecinos[node], j);
      u32 col_vecino = G->color[vecino];      
      if (col_vecino != (u32)~0 && !vis[G->color[vecino]]) {
        vis[G->color[vecino]] = true;
        if(!vector_add(usados, col_vecino)){
            free(vis);
            vector_free(usados);
            return (u32)~0;
        }
      }
    }
    //Se iteran por los colores se queda con el primero que no haya sido usado
    u32 c = 0;
		while(vis[c]) c++;
    //Se asigna este color al nodo
		G->color[G->orden[i]] = c;
    //Se actualiza el coloreo propio
    if(c + 1 > G->c_propio){
		  G->c_propio = c+1;
      if(!vector_add(G->n_color,0)){
        //Se agrega el color al vector si no se existia anteriormente
        free(vis);
        vector_free(usados);
        return (u32)~0;
      }
    }
    //Se incrementa la cantidad de nodos con ese color
    vector_plusplus(G->n_color,c);
		while(vector_size(usados) > 0) {
      vis[vector_back(usados)] = 0;
      vector_pop(usados);
    }
  }

  //Se liberan los recursos
  free(vis);
  vector_free(usados);

  return G->c_propio;
}

/**
  * Verifica para todas las componentes del grafo sean bipartitos, si una
  * componente no es bipartito, corre Greedy en el orden natural, libera
  * memoria y retorna 0.
  * En caso de ser bipartito, asigna los colores en la estructura del grafo,
  * y en el vector n_color de la estructura, suma 1 en la posicion del color 
  * del nodo actual. Luego asigno 2 a la cantidad de colores en la estructura
  */
int Bipartito(Grafo G) {
  
  if(!G)
    return -1;
  u32 n = G->n;
  stack_t stack = create_stack();
  if(!stack)
    return -1;
  u32 *color =  calloc(n, sizeof(u32));

  if(!color){
    destroy_stack(stack);
    return -1;
  }

  for(u32 i = 0; i < n; i++) {
    color[i] = ~0;
  }

  u32 node = 0;
  // Para cada componente
  for(node = 0; node < G->n; node++) {

    // Si todavia no esta coloreado
    if (color[node] < (u32)~0)
      continue;

    // Pusheo al stack, y asigno color 0
    if(!push(node, stack)){
      free(color);
      destroy_stack(stack);
      return -1; 
    }

    color[node] = 0;
    u32 c_color = color[node];

    while (!is_empty_stack(stack)){
      u32 actual = top(stack);
      pop(stack);
      c_color = color[actual];
      // Para cada uno de los vecinos del nodo actual

      for(u32 j = 0; j < vector_size(G->vecinos[actual]); j++){
        u32 vecino = vector_get(G->vecinos[actual], j);
        // Si no esta coloreado, lo agrego al stack, y le asigno el color 
        // opuesto al padre


        if (color[vecino] == (u32)~0){
          if(!push(vecino, stack)){
            free(color);
            destroy_stack(stack);
          }
          color[vecino] = (c_color+1)%2;
        }
        // Si ya estaba coloreado, reviso que el color del vecino sea
        // distinto que el color del nodo actual.
        // Si son iguales corro Greedy en orden natural, libero memoria, y
        // devuelvo 0.
        if (color[vecino] < (u32)~0 && color[vecino] == color[actual]) {
          OrdenNatural(G);
          Greedy(G);
          destroy_stack(stack);
          free(color);
          return false;
        }
      }
    }
  }
  vector_clear(G->n_color);
  vector_add(G->n_color,0);
  vector_add(G->n_color,0);

  // Asigno los colores a los nodos y sumo 1 en la posicion del color que
  // estoy asignando en el momento.
  for(u32 i = 0; i < G->n; i++) {
    assert(color[i] < (u32)~0);
    G->color[i] = color[i];
    vector_plusplus(G->n_color, color[i]);
  }
  G->c_propio = 2;
  
  // Libero memoria
  destroy_stack(stack);
  free(color);

  return 1;
}

/* Funciones para extraer informacion de datos del G */

/**
  * Verifica que el coloreo actual sea propio recorriendo para cada nodo, 
  * todos sus vecinos, y verificando que sean colores diferentes
  */
bool EsPropio(Grafo G){
  u32 n = G->n;
  for(u32 i = 0; i < n; i++) {
    u32 m = GradoDelVertice(G, i);
    u32 node = G->orden[i];
    for(u32 j = 0; j < m; j++) {
      u32 color_vecino = ColorJotaesimoVecino(G, i, j);
      if (G->color[node] == color_vecino) {
        printf("No es propio\nError en %d %d\n", G->color[node], color_vecino);
        return false;
      }
    }
  }
  return true;
}

u32 NumeroDeVertices(Grafo G) {
  return G->n;
}

u32 NumeroDeLados(Grafo G) {
  return G->m;
}

u32 NumeroDeColores(Grafo G) {
  return G->c_propio;
}

/* Funciones de los vertices */
u32 NombreDelVertice(Grafo G, u32 i) {
  return G->nombre_real[G->orden[i]];
}

u32 ColorDelVertice(Grafo G, u32 i) {
  if (i >= G->n) return ~0;
  return G->color[G->orden[i]];
}

u32 GradoDelVertice(Grafo G, u32 i) {
  if (i >= G->n) return ~0u;
  return vector_size(G->vecinos[G->orden[i]]);
}

u32 ColorJotaesimoVecino(Grafo G, u32 i, u32 j) {
  if (i >= G->n || j >= GradoDelVertice(G, i)) {
    return ~0;
  }
  return G->color[vector_get(G->vecinos[G->orden[i]], j)];
}

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j) {
  return vector_get(G->vecinos[G->orden[i]], j);
}


/* Funciones de ordenacion */

/**
  * En nuestra estructura tenemos guardado el orden real para el OrdenNatural,
  * entonces simplemente lo copiamos en orden[i].
  */

char OrdenNatural(Grafo G) {
  for(u32 i = 0; i < G->n; i++) {
    G->orden[i] = G->orden_real[i];
  }
  return '0';
}


/**
  * Creamos un arreglo de vectores, donde el vector[i] tiene todos los 
  * nodos de grado i, finalmente recorremos todos los vectores desde el
  * maximo grado de los nodos hasta 1 y los vamos metiendo en el arreglo
  * del orden en la estructura
  */
char OrdenWelshPowell(Grafo G) {
  u32 n = G->n;
  vector *nodes = (vector*)malloc(n * sizeof(vector));
  if (nodes == NULL) return '1';          
  for(u32 i = 0; i < n; i++) {
    nodes[i] = vector_init();
  }

  // Agrego los nodos en los vectores y hago un max de todos los grados.
  u32 maxgrade = 0;
  for(u32 i = 0; i < n; i++) {
    if(!vector_add(nodes[GradoDelVertice(G, i)], G->orden[i])){
      free(nodes);
      return '1';
    }
    maxgrade = max(maxgrade, GradoDelVertice(G, i));
  }

  // Asigno los nodos en el arreglo de orden de la estructura
  u32 k = 0;
  for(u32 i = maxgrade; i+1 > 0; i--) {
    for(u32 j = vector_size(nodes[i])-1; j+1 > 0; j--) {
      G->orden[k] = vector_get(nodes[i], j);
      k++;     
    }
  }

  // Libero memoria
  for(u32 i = 0; i < n; i++) {
    vector_free(nodes[i]);
  }
  free(nodes);

  return '0';
}

/**
  * Revisa que los indices esten en el rango correcto, y hace un swap en el
  * arreglo de orden.
  */
char SwitchVertices(Grafo G, u32 i, u32 j) {
  u32 n = G->n, v = 0;
  if (i >= n || j >= n) {
    return '1';
  } else {
    v = G->orden[i];
    G->orden[i] = G->orden[j];
    G->orden[j] = v;
    return '0';
  }
}


/**
  * Creamos un arreglo de vectores, donde el vector[c] tiene todos los 
  * nodos con color c, finalmente recorremos todos los vectores desde 0 hasta
  * hasta la cantidad de colores en ese momento, y vamos metiendo los nodos en
  * el arreglo del orden en la estructura
  */

char RMBCnormal(Grafo G) {

  vector *nodes = (vector*)malloc((NumeroDeColores(G)+1) * sizeof(vector));
  if (nodes == NULL) return '1';
  for(u32 i = 0; i <= NumeroDeColores(G); i++) {
    nodes[i] = vector_init();
  }

  // Agrego los nodos en los vectores
  for(u32 i = 0; i < G->n; i++) {
    u32 vertice = G->orden[i];
    if(!vector_add(nodes[ColorDelVertice(G, i)], vertice)){
      free(nodes);
      return '1';
    }
  }

  // Asigno los nodos en el arreglo de orden de la estructura
  u32 k = 0;
  for(u32 i = 0; i <= NumeroDeColores(G); i++) {
    for(u32 j = 0; j < vector_size(nodes[i]); j++) {
      G->orden[k] = vector_get(nodes[i], j);
      k++;     
    }
  }

  // Libero memoria      
  for(u32 i = 0; i <= NumeroDeColores(G); i++) {
    vector_free(nodes[i]);
  }

  free(nodes);
  
  return '0';

}

/**
  * Llamamos a RMBCnormal, y revertimos el orden del arreglo.
  */
char RMBCrevierte(Grafo G) {
  if (RMBCnormal(G) == '1') return '1';
  u32 n = G->n, t;
  
  for(u32 i = 0; i < n/2; i++) {
    t = G->orden[i];
    G->orden[i] = G->orden[n-1-i];
    G->orden[n-1-i] = t;
  }

  return '0';

}

/** Primero se crea un arreglo de vectores de colores
  * y se agrupan los nodos por color
  * Luego, se ordenan los vectores de colores en 
  * funcion de la cantidad de elementos coloreados
  * y se aplica ese orden al orden de los nodos
  */
char RMBCchicogrande(Grafo G) {
  if(!G)
    return '1';
  
  u32 n = G->n, node, cant;
  u32 c = NumeroDeColores(G);
  vector *sizes   = calloc(n,sizeof(vector));
  //Esto es O(n)
  if(!sizes){
    return '1';
  }
  vector *colores = calloc(c,sizeof(vector));
  // Esto es O(c)
  if(!colores){
    free(sizes);
    return '1';
  }

  u32 z = 0, color=0;
  while(z<n || color < c){
    if(z < n)
      sizes[z] = vector_init();
    if(color < c)
      colores[color] = vector_init();
    z++;
    color++;
  }

  for(u32 i = 0; i < n; i++) {
    node = G->orden[i];
    color = G->color[node];
    if(!vector_add(colores[color],node)){
      for(u32 h = 0 ; h < n;h++){
        vector_free(sizes[h]);
      }
      for(u32 h = 0 ; h < c;h++){
        vector_free(colores[h]);
      }
      free(sizes);
      free(colores);
      return '1';
    }
  }

  // En size[i] esta el vector con todos los nodos coloreados con un color
  // con i elementos   
  for(u32 i = 0; i< c; i++){
    vector vec = colores[i];
    u32 t = vector_size(vec);
    for(u32 j = 0; j< t;j++){
      node = vector_get(vec,j);
      color = G->color[node];
      cant = CantidadNodosConColor(G,color);
      if(!vector_add(sizes[cant - 1],node)){
        for(u32 k = 0 ; k < n;k++){
          vector_free(sizes[k]);
        }
        for(u32 k = 0 ; k < c;k++){
          vector_free(colores[k]);
        }
        free(sizes);
        free(colores);
        return '1';
      }
    }
  }

  // Asigno los nodos en el arreglo de orden de la estructura
  u32 k = 0;
  for(u32 i = 0; i < n; i++) {
    for(u32 j = 0; j < vector_size(sizes[i]); j++) {
      G->orden[k] = vector_get(sizes[i], j);
      k++;
    }
  }

  // Libero memoria 
  for(u32 i = 0; i< c; i++){
      vector_free(colores[i]);
  }
  for(u32 i = 0; i < n; i++) {
      vector_free(sizes[i]);
  }
  free(colores);
  free(sizes);

  return '0';
}

/**
  * Recorre todos nodos y los que tienen el color i, los cambia a j y
  * viceversa, al final del todo intercambia los valores de G->n_color[i]
  * con G->n_color[j] y viceversa.
  */
char SwitchColores(Grafo G, u32 i, u32 j) {
  u32 c = NumeroDeColores(G), n_ci, n_cj, color; 
  if (i >= c || j >= c) {
    return '1';
  } else {
    n_ci = CantidadNodosConColor(G, i);
    n_cj = CantidadNodosConColor(G, j);
    for(u32 k = 0; k < G->n; k++) {
      color = ColorDelVertice(G, k);
      u32 vertice = NumeroDelVertice(G,k);
      if (color == i) {
        G->color[vertice] = j;
      } else if (color == j) {
        G->color[vertice] = i;
      }
    }
    vector_set(G->n_color, i, n_cj);
    vector_set(G->n_color, j, n_ci);
    return '0';
  }
}

/* Funciones extras */

void OrderDump(Grafo g){
  for(u32 i = 0; i < NumeroDeVertices(g); i++){
    u32 vertice = NumeroDelVertice(g,i);
    printf("Vertice %u. Color: %d.\n", vertice, ColorDelVertice(g,vertice));
  }
  printf("\n");
}

u32 CantidadNodosConColor(Grafo G, u32 color) {
  return vector_get(G->n_color, color);
}

u32 NumeroDelVertice(Grafo G, u32 i) {
  return G->orden[i];
}